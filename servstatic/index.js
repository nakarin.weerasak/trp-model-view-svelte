const express = require("express");
const app = express();

const port = process.env.PORT || 7000;

app.use("/", (req, res, next) => {
    return express.static("../build")(req, res, next);
});

app.listen(port, "0.0.0.0", () => {
    console.log(`app listening at http://localhost:${port}`);
});